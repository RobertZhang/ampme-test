package com.ampmetest.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ampmetest.test.model.Playlist;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by robertzzy on 16/08/16.
 */
public class MainActivity extends AppCompatActivity {

	protected RecyclerView playlistList;

	protected TextView placeHolder;

	protected SwipeRefreshLayout mainContainer;

	protected PlaylistManager manager;

	protected Subscription loadSubscription;
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		playlistList = (RecyclerView) findViewById(R.id.playlist_list);
		mainContainer = (SwipeRefreshLayout) findViewById(R.id.main_layout);
		placeHolder = (TextView) findViewById(R.id.place_holder);
		playlistList.setAdapter(new PlaylistAdapter());
		playlistList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		manager = new PlaylistManager();
		manager.init();
		mainContainer.setRefreshing(false);
		mainContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				refresh();
			}
		});
	}

	protected void refresh(){
		((PlaylistAdapter) playlistList.getAdapter()).clear();
		mainContainer.setRefreshing(true);
		loadSubscription = manager.generateListFromFacebook(MainActivity.this)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.doOnUnsubscribe(new Action0() {
					@Override
					public void call() {
						mainContainer.setRefreshing(false);
					}
				})
				.subscribe(new Action1<List<Playlist>>() {
					@Override
					public void call(List<Playlist> playlists) {
						if(placeHolder.getVisibility() == View.VISIBLE){
							placeHolder.setVisibility(View.GONE);
						}
						((PlaylistAdapter) playlistList.getAdapter()).addPlaylists(playlists);
					}
				}, new Action1<Throwable>() {
					@Override
					public void call(Throwable throwable) {
						mainContainer.setRefreshing(false);
					}
				}, new Action0() {
					@Override
					public void call() {
						mainContainer.setRefreshing(false);
					}
				});
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(loadSubscription!= null && !loadSubscription.isUnsubscribed()){
			loadSubscription.unsubscribe();
		}
	}

	@Override
	public void onBackPressed() {
		if(loadSubscription!= null && !loadSubscription.isUnsubscribed()){
			loadSubscription.unsubscribe();
		}else{
			super.onBackPressed();
		}
	}


}
