package com.ampmetest.test;

import android.content.Context;
import android.text.TextUtils;

import com.ampmetest.test.model.LikeResponse;
import com.ampmetest.test.model.Playlist;
import com.ampmetest.test.network.FacebookEndpoint;
import com.ampmetest.test.network.SoundCloudEndpoint;
import com.facebook.AccessToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by robertzzy on 16/08/16.
 */
public class PlaylistManager {

	protected FacebookEndpoint facebookEndpoint;
	protected SoundCloudEndpoint soundCloudEndpoint;

	public void init(){

		Retrofit facebook = new Retrofit.Builder()
				.baseUrl("https://graph.facebook.com")
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		facebookEndpoint = facebook.create(FacebookEndpoint.class);


		Retrofit soundCloud = new Retrofit.Builder()
				.baseUrl("http://api.soundcloud.com")
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.build();

		soundCloudEndpoint = soundCloud.create(SoundCloudEndpoint.class);
	}

	protected Observable<List<Playlist>> generateListFromFacebook(final Context context){
		Map<String, String> queryMap = new HashMap<>();
		queryMap.put("fields", "genre");
		queryMap.put("limit", "1000");
		queryMap.put("access_token", AccessToken.getCurrentAccessToken().getToken());
		return facebookEndpoint
				.getPlayList(queryMap)
				.subscribeOn(Schedulers.io())
				.observeOn(Schedulers.computation())
				.flatMap(new Func1<LikeResponse, Observable<List<Playlist>>>() {
					@Override
					public Observable<List<Playlist>> call(LikeResponse likeResponse) {
						List<Observable<List<Playlist>>> playlistFetchList = new ArrayList<>();
						for(LikeResponse.Genre genre: likeResponse.getData()){
							if(!TextUtils.isEmpty(genre.getGenre())){
								playlistFetchList.add(soundCloudEndpoint.getPlayList(genre.getGenre(), context.getString(R.string.sound_cloud_id), 200).subscribeOn(Schedulers.io()));
							}
						}
						return Observable.merge(playlistFetchList, 3);
					}
				});
	}

}
