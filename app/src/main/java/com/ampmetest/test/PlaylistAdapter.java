package com.ampmetest.test;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampmetest.test.model.Playlist;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robertzzy on 16/08/16.
 */
public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistView> {

	protected List<Playlist> playlists = new ArrayList<>();

	public void addPlaylists(List<Playlist> playlists){
		int insertPosition = playlists.size();
		int totalAdded = 0;
		for(Playlist playlist : playlists){
			if(!this.playlists.contains(playlist)){
				this.playlists.add(playlist);
				++totalAdded;
			}
		}
		notifyItemRangeInserted(insertPosition, totalAdded);
	}
	public void clear(){
		int removed = playlists.size();
		this.playlists.clear();
		notifyItemRangeRemoved(0, removed);
	}
	@Override
	public PlaylistView onCreateViewHolder(ViewGroup parent, int viewType) {
		return new PlaylistView(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_playlist, parent, false));
	}

	@Override
	public void onBindViewHolder(PlaylistView holder, int position) {
		holder.title.setText(playlists.get(position).getTitle());
		if(!TextUtils.isEmpty(playlists.get(position).getAlbumCover())){
			holder.logo.setImageURI(Uri.parse(playlists.get(position).getAlbumCover()));
		}
	}

	@Override
	public int getItemCount() {
		return playlists.size();
	}

	protected static final class PlaylistView extends RecyclerView.ViewHolder{
		protected SimpleDraweeView logo;
		protected TextView title;
		public PlaylistView(View itemView) {
			super(itemView);
			logo = (SimpleDraweeView) itemView.findViewById(R.id.album_logo);
			GenericDraweeHierarchyBuilder builder =
					new GenericDraweeHierarchyBuilder(itemView.getResources());
			GenericDraweeHierarchy hierarchy = builder
					.setFadeDuration(300)
					.setPlaceholderImage(itemView.getResources().getDrawable(R.color.grey))
					.build();
			logo.setHierarchy(hierarchy);
			logo.setAspectRatio(1.0f);
			title = (TextView) itemView.findViewById(R.id.album_title);
		}
	}
}
