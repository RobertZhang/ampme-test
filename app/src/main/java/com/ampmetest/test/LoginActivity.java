package com.ampmetest.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity{
	protected LoginButton loginButton;
	protected CallbackManager callbackManager;
	protected Button toMainButton;
	private AccessTokenTracker accessTokenTracker;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		loginButton = (LoginButton) findViewById(R.id.login_button);
		toMainButton = (Button) findViewById(R.id.to_main_button);
		callbackManager = CallbackManager.Factory.create();
		loginButton.setReadPermissions(Arrays.asList("user_likes"));
		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {
				toMain();
				Toast.makeText(LoginActivity.this, R.string.login_toast, Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onCancel() {
				Toast.makeText(LoginActivity.this, R.string.login_cancel_toast, Toast.LENGTH_SHORT).show();

			}

			@Override
			public void onError(FacebookException error) {
				Toast.makeText(LoginActivity.this, R.string.login_failed_toast, Toast.LENGTH_SHORT).show();

			}
		});
		toMainButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toMain();
			}
		});

		accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
			                                           AccessToken currentAccessToken) {
				if (currentAccessToken == null) {
					toMainButton.setVisibility(View.GONE);
				}
			}
		};
		if(isLoggedIn()){
			toMain();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		accessTokenTracker.stopTracking();
	}

	protected void toMain(){
		Intent intent = new Intent(LoginActivity.this, MainActivity.class);
		startActivity(intent);
	}
	@Override
	protected void onResume() {
		super.onResume();
		if(isLoggedIn()){
			toMainButton.setVisibility(View.VISIBLE);
		}else{
			toMainButton.setVisibility(View.GONE);
		}
	}

	protected boolean isLoggedIn(){
		return Profile.getCurrentProfile()!= null && AccessToken.getCurrentAccessToken() != null;
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

}

