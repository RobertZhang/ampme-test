package com.ampmetest.test.model;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by robertzzy on 16/08/16.
 */
public class Playlist {

	@Expose
	@SerializedName("title")
	String title;

	@Expose
	@SerializedName("artwork_url")
	String albumCover;

	@Expose
	@SerializedName("tracks")
	List<Track> tracks;

	public static class Track{
		@Expose
		@SerializedName("artwork_url")
		String albumCover;

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("Track{");
			sb.append("albumCover='").append(albumCover).append('\'');
			sb.append('}');
			return sb.toString();
		}
	}

	public String getTitle() {
		return title;
	}

	public String getAlbumCover() {
		if(TextUtils.isEmpty(albumCover)){
			for(Track track: tracks){
				if(TextUtils.isEmpty(track.albumCover)){
					continue;
				}
				return track.albumCover;
			}
			return albumCover;
		}
		return albumCover;

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (! (o instanceof Playlist)) return false;

		Playlist playlist = (Playlist) o;

		return getTitle().equals(playlist.getTitle());

	}

	@Override
	public int hashCode() {
		return getTitle().hashCode();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Playlist{");
		sb.append("title='").append(title).append('\'');
		sb.append(", albumCover='").append(albumCover).append('\'');
		sb.append(", tracks=").append(tracks);
		sb.append('}');
		return sb.toString();
	}
}
