package com.ampmetest.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by robertzzy on 16/08/16.
 */
public class LikeResponse {

	@Expose
	@SerializedName("data")
	List<Genre> data;


	public static class Genre{
		@Expose
		@SerializedName("genre")
		String genre;

		public String getGenre() {
			return genre;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder("Genre{");
			sb.append("genre='").append(genre).append('\'');
			sb.append('}');
			return sb.toString();
		}
	}

	public List<Genre> getData() {
		return data;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("LikeResponse{");
		sb.append("data=").append(data);
		sb.append('}');
		return sb.toString();
	}
}
