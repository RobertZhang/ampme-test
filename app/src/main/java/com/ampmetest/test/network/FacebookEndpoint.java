package com.ampmetest.test.network;

import com.ampmetest.test.model.LikeResponse;

import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by robertzzy on 16/08/16.
 */
public interface FacebookEndpoint {
	@GET("/v2.7/me/music")
	Observable<LikeResponse> getPlayList(@QueryMap Map<String, String> options);
}
