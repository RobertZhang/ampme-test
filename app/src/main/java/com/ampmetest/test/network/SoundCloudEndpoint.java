package com.ampmetest.test.network;

import com.ampmetest.test.model.Playlist;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by robertzzy on 16/08/16.
 */
public interface SoundCloudEndpoint {
	@GET("/playlists")
	Observable<List<Playlist>> getPlayList(@Query("genre")String genre, @Query("client_id")String clientId, @Query("limit") int limit);
}
