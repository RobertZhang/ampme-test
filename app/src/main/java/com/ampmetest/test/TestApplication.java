package com.ampmetest.test;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by robertzzy on 16/08/16.
 */
public class TestApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		FacebookSdk.sdkInitialize(this);
		Fresco.initialize(this);

	}
}
